﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace JiraRestClient.Tests
{
    [TestFixture]
    public class GetIssueTests
    {

        [Test]
        public void WhenValidIssueIsRetrievedIsOfTypeIJiraIssue()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";

            var issue = client.GetIssue(key);
            Assert.IsInstanceOf<IJiraIssue>(issue);
        }

        [Test]
        public void WhenValidIssueIsRetrievedIsOfTypeJiraIssue()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";

            var issue = client.GetIssue(key);
            Assert.IsInstanceOf<JiraIssue>(issue);
        }

        [Test]
        public void WhenParentIssueIsRetrievedIsSubTaskIsFalse()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";

            IJiraIssue issue = client.GetIssue(key);
            Assert.IsFalse(issue.IsSubtask);
        }

        [Test]
        public void WhenSubTaskIssueIsRetrievedIsSubTaskIsTrue()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-2";

            IJiraIssue issue = client.GetIssue(key);
            Assert.IsTrue(issue.IsSubtask);
        }

        [Test]
        public void WhenAIssueWithTypeBugIsRetrievedIssueTypeIsBug()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";

            var issue = client.GetIssue(key);
            Assert.AreEqual("Bug", issue.IssueType);
        }

        [Test]
        public void WhenAIssueWithTypeTaskIsRetrievedIssueTypeIsTask()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-3";

            var issue = client.GetIssue(key);
            Assert.AreEqual("Task", issue.IssueType);
        }

        [Test]
        public void WhenAIssueWithTypeImprovementIsRetrievedIssueTypeIsImprovement()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-5";

            var issue = client.GetIssue(key);
            Assert.AreEqual("Improvement", issue.IssueType);
        }

        [Test]
        public void WhenAIssueWithTypeSubTaskIsRetrievedIssueTypeIsSubTask()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-2";

            var issue = client.GetIssue(key);
            Assert.AreEqual("Sub-task", issue.IssueType);
        }

        [Test]
        public void WhenAIssueWithTypeNewFeatureIsRetrievedIssueTypeIsNewFeature()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-4";

            var issue = client.GetIssue(key);
            Assert.AreEqual("New Feature", issue.IssueType);
        }

        [Test]
        public void WhenIssueIsRetrievedIssueTypeObjectIsOfTypeIJiraIssueType()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";

            var issue = client.GetIssue(key);
            Assert.IsInstanceOf<IJiraIssueType>(issue.IssueTypeObject);
        }

        [Test]
        public void WhenIssueIsRetrievedIssueTypeObjectIsOfTypeJiraIssueType()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";

            var issue = client.GetIssue(key);
            Assert.IsInstanceOf<JiraIssueType>(issue.IssueTypeObject);
        }

        [Test]
        public void WhenIssueIsRetrievedKeyIsSet()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";

            IJiraIssue issue = client.GetIssue(key);
            Assert.AreEqual(key, issue.Key);
        }

        [Test]
        public void WhenIssueWithLabelsIsRetrievedLabelsAreAsExpected()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";
            IEnumerable<string> expectedLabels = new List<string> { "ReleaseNotes", "UI" };

            var issue = client.GetIssue(key);
            IEnumerable<string> actualLabels = from label in issue.Labels orderby label select label;
            CollectionAssert.AreEqual(expectedLabels, actualLabels);
        }

        [Test]
        public void WhenIssueWithoutLabelsIsRetrievedLabelsEmpty()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-2";

            var issue = client.GetIssue(key);
            IEnumerable<string> actualLabels = from label in issue.Labels orderby label select label;
            CollectionAssert.IsEmpty(actualLabels);
        }

        [Test]
        public void WhenParentIssueIsRetrievedParentIsNull()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";

            var issue = client.GetIssue(key);
            Assert.IsNull(issue.Parent);
        }

        [Test]
        public void WhenSubTaskIssueIsRetrievedParentIsNotNull()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-2";

            var issue = client.GetIssue(key);
            Assert.IsNotNull(issue.Parent);
        }

        [Test]
        public void WhenSubTaskIssueIsRetrievedParentIsParentKey()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-2";
            var parentKey = "JRCTES-1";

            var issue = client.GetIssue(key);
            Assert.AreEqual(parentKey, issue.Parent);
        }

        [Test]
        public void WhenParentIssueIsRetrievedParentObjectKeyIsNull()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";

            var issue = client.GetIssue(key);
            Assert.IsNull(issue.ParentObject.Key);
        }

        [Test]
        public void WhenSubTaskIssueIsRetrievedParentObjectIsNotNull()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-2";

            var issue = client.GetIssue(key);
            Assert.IsNotNull(issue.ParentObject);
        }

        [Test]
        public void WhenSubTaskIssueIsRetrievedParentObjectIsOfTypeIJiraIssue()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-2";

            var issue = client.GetIssue(key);
            Assert.IsInstanceOf<IJiraIssue>(issue.ParentObject);
        }

        [Test]
        public void WhenSubTaskIssueIsRetrievedParentObjectIsOfTypeJiraIssue()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-2";

            var issue = client.GetIssue(key);
            Assert.IsInstanceOf<JiraIssue>(issue.ParentObject);
        }

        [Test]
        public void WhenSubTaskIssueIsRetrievedParentObjectKeyIsTheParentKey()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-2";
            var parentKey = "JRCTES-1";

            var issue = client.GetIssue(key);
            Assert.AreEqual(parentKey, issue.ParentObject.Key);
        }

        [Test]
        public void WhenIssueIsRetrievedSelfIsCorrectURL()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";
            var expectedSelf = "http://localhost:2990/jira/rest/api/latest/issue/10000";

            var issue = client.GetIssue(key);
            Assert.AreEqual(expectedSelf, issue.Self);
        }

        [Test]
        public void WhenIssueIsRetrievedSummaryIsCorrect()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var key = "JRCTES-1";
            var expectedSummary = "A Bug";

            var issue = client.GetIssue(key);
            Assert.AreEqual(expectedSummary, issue.Summary);
        }
    }
}
