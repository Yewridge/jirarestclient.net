﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace JiraRestClient.Tests
{
    [TestFixture]
    class GetIssueTypeByResourceTests
    {
        [Test]
        public void WhenIssueTypeIsRetrievedThenIsOfTypeIJiraIssueType()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var issueType = client.GetIssueTypeByResource("http://localhost:2990/jira/rest/api/latest/issuetype/1");

            Assert.IsInstanceOf<IJiraIssueType>(issueType);
        }

        [Test]
        public void WhenIssueTypeIsRetrievedThenIsOfTypeJiraIssueType()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var issueType = client.GetIssueTypeByResource("http://localhost:2990/jira/rest/api/latest/issuetype/1");

            Assert.IsInstanceOf<JiraIssueType>(issueType);
        }

        [TestCase(1, "Bug")]
        [TestCase(2, "New Feature")]
        [TestCase(3, "Task")]
        [TestCase(4, "Improvement")]
        [TestCase(5, "Sub-task")]
        public void WhenIssueTypeIsRetrievedThenNameIsCorrect(int id, string name)
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var issueType = client.GetIssueTypeByResource(string.Format("http://localhost:2990/jira/rest/api/latest/issuetype/{0}",id));

            Assert.AreEqual(name,issueType.Name);
        }
    }
}
