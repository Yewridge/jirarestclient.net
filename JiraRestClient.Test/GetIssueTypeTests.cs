﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
    
namespace JiraRestClient.Tests
{
    [TestFixture]
    public class GetIssueTypeTests
    {
        [Test]
        public void WhenIssueTypeIsRetrievedThenIsOfTypeIJiraIssueType()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var issueType = client.GetIssueType(1);

            Assert.IsInstanceOf<IJiraIssueType>(issueType);
        }

        [Test]
        public void WhenIssueTypeIsRetrievedThenIsOfTypeJiraIssueType()
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var issueType = client.GetIssueType(1);

            Assert.IsInstanceOf<JiraIssueType>(issueType);
        }

        [TestCase(1, "A problem which impairs or prevents the functions of the product.")]
        [TestCase(2, "A new feature of the product, which has yet to be developed.")]
        [TestCase(3, "A task that needs to be done.")]
        [TestCase(4, "An improvement or enhancement to an existing feature or task.")]
        [TestCase(5, "The sub-task of the issue")]
        public void WhenIssueTypeIsRetrievedThenDescriptionIsCorrect(int id, string description)
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var issueType = client.GetIssueType(id);

            Assert.AreEqual(description, issueType.Description);
        }

        [TestCase(1, "http://localhost:2990/jira/images/icons/issuetypes/bug.png")]
        [TestCase(2, "http://localhost:2990/jira/images/icons/issuetypes/newfeature.png")]
        [TestCase(3, "http://localhost:2990/jira/images/icons/issuetypes/task.png")]
        [TestCase(4, "http://localhost:2990/jira/images/icons/issuetypes/improvement.png")]
        [TestCase(5, "http://localhost:2990/jira/images/icons/issuetypes/subtask_alternate.png")]
        public void WhenIssueTypeIsRetrievedThenIconUrlIsCorrect(int id, string iconUrl)
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var issueType = client.GetIssueType(id);

            Assert.AreEqual(iconUrl, issueType.IconUrl);
        }

        [TestCase(1, "Bug")]
        [TestCase(2, "New Feature")]
        [TestCase(3, "Task")]
        [TestCase(4, "Improvement")]
        [TestCase(5, "Sub-task")]
        public void WhenIssueTypeIsRetrievedThenNameIsCorrect(int id, string name)
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var issueType = client.GetIssueType(id);

            Assert.AreEqual(name, issueType.Name);
        }

        [TestCase(1, false)]
        [TestCase(2, false)]
        [TestCase(3, false)]
        [TestCase(4, false)]
        [TestCase(5, true)]
        public void WhenIssueTypeIsRetrievedThenIsSubtaskIsCorrect(int id, bool subtask)
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var issueType = client.GetIssueType(id);

            Assert.AreEqual(subtask, issueType.IsSubtask);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5 )]
        public void WhenIssueTypeIsRetrievedThenSelfIsCorrect(int id)
        {
            var client = JiraRestClientHelpers.GetJiraClient();
            var issueType = client.GetIssueType(id);
            var self = string.Format(@"http://localhost:2990/jira/rest/api/latest/issuetype/{0}", id);
            Assert.AreEqual(self, issueType.Self);
        }

    }
}
