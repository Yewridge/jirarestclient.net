﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JiraRestClient.Tests
{
    public static class JiraRestClientHelpers
    {
        public static IJiraRestClient GetJiraClient()
        {
            return new JiraRestClient("http://localhost:2990/jira", "admin", "admin");
        }
    }
}
